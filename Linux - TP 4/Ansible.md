# Introduction Infrastructure as Code : Ansible
[[_TOC_]]
## Création de playbooks
### Déploiement base de données : MariaDB
```yaml
---
- name: Install mariadb
  hosts: cesi
  become: true

  tasks:
  - name: Add epel-release repo
    yum:
      name: epel-release
      state: present

  - name: Install mariadb-server
    yum:
      name: mariadb-server
      state: present

  - name: Install pymysql
    pip:
      name: pymysql
      state: present

  - name: Start mariadb
    service:
      name: mariadb
      state: started

  - name: add db
    mysql_db:
      login_user: root
      name: db_test
      state: present

```
mysql_secure_install n'est pas fait automatiquement pour le faire il faut automatiser chaque étape via ansible
### Déploiement comptes utilisateurs
```yaml
---
- name: add user
  hosts: cesi
  become: true

  tasks:
  - name: Add the user chips
    user:
      name: chips
      group: wheel
      state: present
      password: "{{ 'test' | password_hash('sha512') }}"

  - name: Set authorized key copying it from current user
    authorized_key:
      user: chips
      state: present
      key: "{{ lookup('file', '/home/christopher/.ssh/id_ecdsa.pub') }}"


```
>key: "{{ lookup('file', '/home/christopher/.ssh/id_ecdsa.pub') }}"

C'est un chemin sur le master node
### Configuration du firewall
```yaml
---
- name: Install nginx
  hosts: cesi
  become: true

  tasks:
  - name: no_8000
    ansible.posix.firewalld:
      port: 8000/tcp
      permanent: yes
      immediate: yes
      state: disabled

  - name: yes_80
    ansible.posix.firewalld:
      port: 80/tcp
      permanent: yes
      immediate: yes
      state: enabled

  - name: yes_22
    ansible.posix.firewalld:
      port: 22/tcp
      permanent: yes
      immediate: yes
      state: enabled

```
## Developement techniques
### Mise en place de tests
Pour faire de la CI/CD il faut "valider" son compte avec la CB dcp flm
Donc on va juste prier pour que ca fonctionne comme ça :
```yaml
stages:
  - test_yaml_syntax

first-test:
  stage: test_yaml_syntax
  image: rockylinux
  script:
    - sudo dnf install epel-release -y
    - sudo dnf install yamllint -y
    - yamllit .
```